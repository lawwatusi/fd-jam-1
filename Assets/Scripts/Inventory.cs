﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory{

    public event EventHandler OnitemListChanged;

    private List<Item> itemList;

    public Inventory()
    {
        itemList = new List<Item>();

       // AddItem(new Item { itemType = Item.ItemType.Cheese, amount = 1 });
        //AddItem(new Item { itemType = Item.ItemType.Guitar, amount = 1 });
       // AddItem(new Item { itemType = Item.ItemType.Cheeto, amount = 1 });
        
        Debug.Log(itemList.Count);
    }

    public void AddItem(Item item)
    {
        itemList.Add(item);
        OnitemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public List<Item> GetItemList()
    {
        return itemList;
    }
}
