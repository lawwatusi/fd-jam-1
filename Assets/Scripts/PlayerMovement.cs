﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    public static PlayerMovement Instance { get; private set; }

    Rigidbody2D rb;
    public float moveSpeed = 1f;
    public float jumpForce;
    bool facingLeft = true;
    bool isGrounded;
    public SpriteRenderer mySpriteRenderer;

    //Inventory stuff
    [SerializeField] private UI_Inventory uiInventory;
    private Inventory inventory;

    //Animation stuff
    private Animator anim;

    bool isRunning;
    bool isJumping;

    void Start()
    {
        Instance = this;

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        isGrounded = true;

        inventory = new Inventory();
        uiInventory.SetInventory(inventory);

        ItemWorld.SpawnItemWorld(new Vector3(1, 0), new Item { itemType = Item.ItemType.Cheese, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(2, 0), new Item { itemType = Item.ItemType.Guitar, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(3, 0), new Item { itemType = Item.ItemType.Cheeto, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(-1, 0), new Item { itemType = Item.ItemType.Cheeto, amount = 1 });
        ItemWorld.SpawnItemWorld(new Vector3(-2, 0), new Item { itemType = Item.ItemType.Cheeto, amount = 1 });
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        ItemWorld itemWorld = collider.GetComponent<ItemWorld>();
        if(itemWorld != null)
        {
            //touching
            inventory.AddItem(itemWorld.GetItem());
            itemWorld.DestroySelf();
        }
    }

    // Update is called once per frame
    void Update()
    {

        Move();
        Jump();

    }

          
    void Move()
    {
        Vector3 charScale = transform.localScale;
        float x = Input.GetAxisRaw("Horizontal");
        float moveBy = x * moveSpeed;
        rb.velocity = new Vector2(moveBy, rb.velocity.y);

        if(x == 0)
        {
            anim.SetBool("isRunning", false);
        }
        else
        {
            anim.SetBool("isRunning", true);
        }

        if (facingLeft)
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                mySpriteRenderer.flipX = true;
                facingLeft = false;
            }
        }
        else
        {
            if (Input.GetAxisRaw("Horizontal") < 0)
            {
                mySpriteRenderer.flipX = false;
                facingLeft = true;
            }
        }


        transform.localScale = charScale;
    }

    void Jump()
    {
        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetBool("isJumping", true);
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                isGrounded = false;
                Debug.Log("jumping");
            }

        }
        
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "ground")
        {
            anim.SetBool("isJumping", false);
            isGrounded = true;
            Debug.Log("on the ground");
;        }
    }

}
