﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    public string sceneToChangeTo;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("touching door");
            ChangeToScene(sceneToChangeTo);

        }
    }
    public void ChangeToScene(string sceneName)
    {
        LevelManager.Instance.LoadScene(sceneName);
    }
}
