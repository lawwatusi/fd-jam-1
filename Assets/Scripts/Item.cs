﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{

    public enum ItemType
    {
        Cheese,
        Guitar,
        Cheeto,

    }

    public ItemType itemType;
    public int amount;

    public Sprite GetSprite()
    {
        switch (itemType)
        {
            default:
            case ItemType.Cheese:       return ItemAssets.Instance.cheeseSprite;
            case ItemType.Guitar:       return ItemAssets.Instance.guitarSprite;
            case ItemType.Cheeto:       return ItemAssets.Instance.cheetoSprite;

        }
    }
}
